import inspect

from discord.ext import commands


class MineBot(commands.Bot):
    def __init__(self, *args, **kwargs):
        self.cleanup = []
        super().__init__(*args, **kwargs)

    def add_cleanup(self, callback):
        self.cleanup.append(callback)

    async def close(self):
        for callback in self.cleanup:
            r = callback()
            # coroutines etc. return an awaitable object
            if inspect.isawaitable(r):
                await r
        await super().close()