import asyncio
import inspect
import itertools
import logging
import os
import re

import discord
from discord.ext import commands

import lib
import heavynode


DISCORD_TOKEN = os.environ['discord_token']
DISCORD_SERVER_ID = 530446700058509323
HEAVYNODE_TOKEN = os.environ['heavynode_token']
# SESSION_COOKIE = os.environ['pterodactyl_session_cookie']
COOKIE_NAME = 'remember_web_59ba36addc2b2f9401580f014c7f58ea4e30989d'
COOKIE_VALUE = os.environ[COOKIE_NAME]


logging.basicConfig()

intents = discord.Intents.default()
intents.members = True
bot = lib.MineBot(command_prefix='!', intents=intents)

hn = heavynode.Client(HEAVYNODE_TOKEN, COOKIE_NAME, COOKIE_VALUE)
bot.add_cleanup(hn.shutdown)


async def is_admin(ctx):
    user = ctx.message.author
    guild = discord.utils.get(bot.guilds, id=DISCORD_SERVER_ID)
    member = guild.get_member(user.id)
    if member is not None:
        for role in member.roles:
            if role.name == 'Admin' or role.name == 'Mod':
                return True
    return False


async def whitelist_cmd(action, player):
    expr = re.compile(f'\[Server thread/INFO\]: .*({player}|player).*(whitelist(ed)?|exist)', re.IGNORECASE)
    resp = await hn.cmd_with_response(f'whitelist {action} {player}', expr)
    _, msg = resp.split('[Server thread/INFO]: ')
    return msg


@bot.command()
@commands.check(is_admin)
async def add(ctx, player):
    """Add a player to the server whitelist. Must use exact Minecraft name."""
    msg = await whitelist_cmd('add', player)
    await ctx.send(msg)


@bot.command()
@commands.check(is_admin)
async def remove(ctx, player):
    """Remove a player from the server whitelist. Must use exact Minecraft name."""
    msg = await whitelist_cmd('remove', player)
    await ctx.send(msg + '.')


@add.error
@remove.error
async def whitelist_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.send('You must be a server admin to use this command.')
    elif isinstance(error.original, (heavynode.HttpError, heavynode.GenericError)):
        await ctx.send('Failed to communicate with server.')
    elif isinstance(error.original, asyncio.exceptions.TimeoutError):
        await ctx.send('Command sent; response inconclusive. Check server output for more information.')
    elif isinstance(error.original, ValueError):
        await ctx.send('Command sent; could not interpret server response.')
    else:
        raise error


bot.run(DISCORD_TOKEN, reconnect=True)
